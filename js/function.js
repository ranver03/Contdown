$(document).ready(function(){
     var tiempo={
     minutos: 2,
     seg: 59
     };

     var tiempo_corriendo = null;

     tiempo_corriendo = setInterval(function(){
          $("#second").text(tiempo.seg < 10 ? '0' + tiempo.seg : tiempo.seg);
          $("#minute").text(tiempo.minutos < 10 ? '0' + tiempo.minutos : tiempo.minutos);
          tiempo.seg--;
          if (tiempo.seg < 0) {
               tiempo.seg = 59;
               tiempo.minutos--;
               if (tiempo.minutos < 0){
                    clearInterval(tiempo_corriendo);
               }
          }
     }, 1000);
});

<!DOCTYPE html>
<html>
     <head>
          <meta charset="utf-8">
          <title>Contdown</title>
          <link href="css/style.css" rel="stylesheet" />
          <script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
     </head>
     <body>
     <div id="timer">
          <div class="container">
               <div id="minute">--</div>
               <div class="divider">:</div>
               <div id="second">--</div>
          </div>
     </div>

          <script src="js/function.js" type="text/javascript"></script>
     </body>
</html>
